# NeboScope

Небоскоп - онлайн платформа сбора и анализа наблюдений облачности из космоса и с Земли 

- [Описание проекта](https://gitlab.com/lorett/neboscope/-/wikis/%D0%9D%D0%B5%D0%B1%D0%BE%D1%81%D0%BA%D0%BE%D0%BF.-%D0%9E%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0)
- [Структура и общие принципы разработки](https://gitlab.com/lorett/neboscope/-/wikis/%D0%A1%D1%82%D1%80%D1%83%D0%BA%D1%82%D1%83%D1%80%D0%B0-%D0%B8-%D0%BF%D1%80%D0%B8%D0%BD%D1%86%D0%B8%D0%BF%D1%8B-%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B8-%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0)


_Прект разрабатывается при поддержке компании [Лоретт](https://lorett.org/)_
