import os
import hashlib

from nocache import nocache
from flask import Flask, session, render_template, url_for, request, flash, redirect, send_from_directory

from flask_login import LoginManager, login_manager, login_required, login_user, logout_user, current_user
from userLogin import User
from datetime import datetime
from dataBase import DB
from imagePreparation import image_load, get_exif, get_usefull
from os.path import join, abspath, dirname
from pathlib import Path
import tools.lib
import geocoder



# configurate
app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 5 * 1024 * 1024 + 1
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
login_manager = LoginManager(app)

DATA_ROOT = join(dirname(abspath(__file__)), "data")  # на уровень выше кода - не смешивать !

host = "localhost"
URL_ROOT = "/data"
version = "0.2.202108"

#print(DATA_ROOT)
extentions = {'png', 'jpg', 'jpeg'}

SERVER_PORT = 8080

dbase = DB()
# https://www.youtube.com/watch?v=L_o0wRaZJdg


# Path("/my/directory").mkdir(parents=True, exist_ok=True)


# generate path and filename
def saveImg(dateFrame, file):
    date, time = dateFrame.split("T")
    date = date.split("-")

    fileName = abspath( join(DATA_ROOT, "origin", date[0], date[1], date[2]) )

    Path(fileName).mkdir(parents=True, exist_ok=True)

    fileName = join( fileName, dateFrame.replace(":", "-") + "_" + str(hashlib.md5((time).encode("utf-8")).hexdigest())[:6] + "." + file.filename.split(".", 1)[1] )

    file.save(fileName)
    
    return fileName, image_load(fileName)


@app.route("/upload", methods=["POST", "GET"])
@login_required
def fileLoad():
    # if POST request
    if request.method == "POST":
        ip = request.remote_addr
        print(ip)
        file = request.files['file']

        # check extentions
        if file and file.filename.split(".")[1].lower() in extentions:
            # db write, if done then true else false
            filename, exif = saveImg(request.form['date'], file)
            lat = round(float(request.form['lat']), 3)
            lon = round(float(request.form["long"]), 3)

            #if exif['GPSLatitude'] != "": lat = round(float(exif['GPSLatitude']), 3)
            #else: lat = 0
            #if exif['GPSLongitude'] != "": lon = round(float(exif['GPSLongitude']), 3)
            #else: lon = 0
            
            location = geocoder.opencage([lat, lon], key="eb6713d9c9fb4a8ab99364aa00e8181c", method='reverse').city
            if type(location) != str:
                location = ""
        

            if dbase.fileLoadBD(current_user.get_id(), lat, lon, filename, exif['CameraInfo'], location, ip, datetime.now().strftime("%Y-%m-%dT%H:%M")):
                return redirect("/profile")
            else:
                print("Write in db err")
                return redirect("/data_load")
        else:
            print("invalid file")
            return redirect("/data_load")
    else:
        print("disconnected")
        return redirect("/home")   


@login_manager.unauthorized_handler
def unauthorized():
    return redirect("/login")


@login_manager.user_loader
def load_user(userID):
    return User().fromDB(userID, dbase)


@app.route(f"{URL_ROOT}/<path:filename>")
def data_static(filename):
    separator = filename[4]
    path, file = filename.rsplit(separator, 1)
    path = join("data", "normal", path)
    print( "\n", path, file, "\n" )
    return send_from_directory( path, file )


@app.route('/photo')
@nocache
def sendPhotos():
    date = request.form['date']


@app.route("/map")
#@login_required
def map_page():
    return render_template("map.html")


@app.route("/ping")
def ping_page():
    return render_template("ping.html", version=version)


@app.route("/")
@login_required
def null_page():
    return redirect("/map")


@app.route('/login')
def login_page():
    if current_user.get_id():
        return redirect("/home")
    else:
        return render_template("login.html")
     
                
@app.route('/registration')
def registration_page():
    if current_user.get_id():
        return redirect("/home")
    else:
        return render_template("registration.html")


@app.route('/home')
@login_required
def home_page():
    return render_template("home.html")


@app.route('/profile')
@login_required
def profile_page():
    return render_template("profile.html")


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect("/login")


@app.errorhandler(404)
def page_not_found(f):
    return render_template("page_not_found.html")


@app.route('/my_images')
@login_required
def my_images_page():
    return render_template("my_images.html", data=data_static)


@app.route('/all_images')
@login_required
def all_images_page():
    #if current_user.is_admin():
    return render_template("all_images.html")
    '''else:
        return redirect("/home")'''


@app.route('/data_load')
@login_required
def data_load_page():
    return render_template("data_load.html")


@app.route('/hello')
def page():
    return redirect("/ping")


@app.route('/authentication', methods=["GET","POST"])
def authentication():
    if request.method == "POST":

        user = dbase.getUserByLogin(request.form['login'])
        password = request.form['password']
        
        if user and dbase.loginDB(user.login, password):
            userlogin = User().create(user)
            login_user(userlogin)

            print(f"\nuser login: {user.login}\nuser password: {password}")
            return redirect("/home")

        else:
            print("Invalid password or login")
            return redirect("/login")

    else:
        return redirect("/login")


@app.route('/registration_check', methods=["GET","POST"])
def registration():
    if request.method == "POST":
        login = request.form['login']
        password = request.form['password']
        email = request.form['email']

        # temporary solution
        if dbase.registrationDB(email=email, login=login, password=password):
            return redirect("/home")
        else:
            return redirect("/registration")


@app.route("/api/upload", methods=["POST"])
def apiFileLoad():
    try:
        if request.method == "POST":
            securityKey = request.values.get('securityKey')
            userid = dbase.getUserByKey(securityKey)
            if userid == '':
                return "Invalid key", 400
            # logic

    except Exception as e:
        print(f"/api/upload: exception in parameters check: {e}")        
        return "Invalid parameters", 400


@app.route("/api/check", methods=["POST"])
def apiCheck():
    try:
        if request.method == "POST":
            securityKey = request.values.get('securityKey')
            userid = dbase.getUserByKey(securityKey)
            if userid == '':
                return "Invalid key", 400
            # logic

    except Exception as e:
        print(f"/api/upload: exception in parameters check: {e}")        
        return "Invalid parameters", 400


@app.route("/api/signal", methods=["POST"])
def apiSignal():
    try:
        if request.method == "POST":
            securityKey = request.values.get('securityKey')
            userid = dbase.getUserByKey(securityKey)
            if userid == '':
                return "Invalid key", 400
            # logic

    except Exception as e:
        print(f"/api/upload: exception in parameters check: {e}")        
        return "Invalid parameters", 400


# rework
"""
@app.route("/api/upload", methods=["POST",'GET'])
def api_fileLoad():
    ''' API endpoint for automated photo upload.
    POST only.
    Request fields:
       file - photo image file blob
       key  - API key for user identification
       ts   - photo timestamp (should contain timezone suffix or "Z" for UTC time). If this parameter is missing, current time is taken by the server
       lat,long - location coordinates
       location - location name
       camera - camera model string
    '''
    #--- check and prepare photo insert parameters
    try:
        #userid
        apikey = request.values.get('key','')
        print('key',apikey)
        userid = dbase.getUserByKey(apikey)
        if userid == '':
            return "Invalid key", 400
        #timestamp
        ts = request.values.get('date',None)
        if ts is None : ts = request.values.get('ts',None)
        if not ts: #use current date
            ts = "{:%Y-%m-%dT%H:%M}".format(datetime.utcnow())
        #coordinates. TODO - take from user profile
        lat = float( request.values.get('lat','0.0') )
        lon = float( request.values.get('long','0.0') )
        #
        camera = request.values.get('camera',None)
        location = request.values.get('location','-')
        print("API upload from {}@{} [{}]: {}".format(apikey,userid,camera,ts))
    except Exception as e:
        print("/api/upload: exception in parameters check: {}".format(e))        
        return "Invalid parameters", 400
    #--
    try:
        if not 'file' in request.files:
            print("upload from  {}@{} has no file".format(apikey,userid)) 
            return "No file in upload data", 400
        thefile = request.files['file']
        src_filename = thefile.filename
        print(lat,lon, src_filename)
        if dbase.fileLoadBD2(userid, ts, datetime.now().strftime("%Y-%m-%dT%H:%M"),lat,lon, src_filename, 
                             saveImg(ts,  thefile),request.remote_addr,location,camera):
            return "OK. Thanks for photo upload", 200
        else:
            return  "Error. 1", 400
    except Exception as e:
        print("/api/upload: exception in fileLoadBD: {}".format(e)) 
        return "Error in file registration", 500 
    return "Unexpected error", 500 
"""



if __name__ == "__main__":
    app.run(host=host, port=SERVER_PORT, debug=True, threaded=True)

