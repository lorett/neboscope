from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS
from os.path import join, abspath
from pathlib import Path
import os




path = '../database/photo'
HEIGHT_N = 500 # Высота отмасштабируемого изображения в пикселах
HEIGHT_S = 250 # Высота маленького (small) в пикселах
HEIGHT_T = 100 # Высота миниатюры (thumbnail) в пикселах

UF_INFO = ['DateTimeOriginal', 'GPSLatitude', 'GPSLongitude'] # Полезные данные из EXIF (кроме камеры)


def image_resize(img, h, w):
    '''
    Для внутреннего использования
    Масштабирует изображение
    img = объект Image
    name = имя
    output_image_path = Директория для изображения
    h, v = высота, ширина
    '''
    img = img.resize((w, h), Image.ANTIALIAS)
    return img


def image_load(fileName):
    '''
    Масштабирует изображение по заданным размерам в две папки:
        photo - изображения высотой HEIGHT_N
        ql - изображения высотой HEIGHT_T
    '''
    image = Image.open( fileName )
    exif = get_exif(fileName)
    
    width, height = image.size
    ratio = width / height

    temp = abspath( join(fileName, "..") )

    Path(temp.replace("origin", "normal")).mkdir(parents=True, exist_ok=True)
    Path(temp.replace("origin", "small")).mkdir(parents=True, exist_ok=True)
    Path(temp.replace("origin", "ql")).mkdir(parents=True, exist_ok=True)

    image_resize(image, HEIGHT_N, int(HEIGHT_N * ratio)).save(fileName.replace("origin", "normal"))
    image_resize(image, HEIGHT_T, int(HEIGHT_S * ratio)).save(fileName.replace("origin", "small"))
    image_resize(image, HEIGHT_T, int(HEIGHT_T * ratio)).save(fileName.replace("origin", "ql"))

    return exif
     

def get_exif(image_path):
    '''
    Возвращает EXIF фотографии в видке словаря
    '''
    image = Image.open(image_path)
    
    if image._getexif():
        exif_all = {
            TAGS[k]: v
            for k, v in image._getexif().items()
            if k in TAGS
        }

        exif_all['DateTimeOriginal'] = exif_all['DateTimeOriginal'][:-3]
        exif_all['DateTimeOriginal'] = exif_all['DateTimeOriginal'].replace(":", "-", 2)
        exif_all['DateTimeOriginal'] = exif_all['DateTimeOriginal'].replace(" ", "T")
        
        if 'GPSInfo' in exif_all:
            exif_all['GPSInfo'] = {
                GPSTAGS[k]: v
                for k, v in exif_all['GPSInfo'].items()
                if k in GPSTAGS
            }

            if exif_all['GPSInfo']['GPSLatitudeRef'] == "S":
                exif_all['GPSLatitude'] = exif_all['GPSInfo']['GPSLatitude'][0] * -1
            else:
                exif_all['GPSLatitude'] = exif_all['GPSInfo']['GPSLatitude'][0] 
                

            if exif_all['GPSInfo']['GPSLongitudeRef'] == "W":
                exif_all['GPSLongitude'] = exif_all['GPSInfo']['GPSLongitude'][0] * -1
            else:
                exif_all['GPSLongitude'] = exif_all['GPSInfo']['GPSLongitude'][0]

            del exif_all['GPSInfo']

    else:
        exif_all = {'DateTimeOriginal':"",
                    'CameraInfo':"",
                    'GPSLatitude':"",
                    'GPSLongitude':""}

    print(exif_all['GPSLatitude'])
    return exif_all


def get_usefull(exif_dictionary):
    uf_exif = dict()
    if 'Make' in exif_dictionary.keys() and 'Model' in exif_dictionary.keys():
        uf_exif['CameraInfo'] = exif_dictionary['Make']+' '+exif_dictionary['Model']
    else: 
        uf_exif['CameraInfo'] = ""
        print('CameraInfo not found')
    for kw in UF_INFO:
        if kw in exif_dictionary.keys():
            uf_exif[kw] = exif_dictionary[kw]
        else:
            print(kw+' not found')

    
    return uf_exif


if __name__ == "__main__":
    print(get_usefull(get_exif("test2.jpg")))

    


