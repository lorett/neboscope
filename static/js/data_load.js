d = new Date();

function success(position){
    const latitude  = position.coords.latitude;
    const longitude = position.coords.longitude;
    console.log(latitude)
    console.log(longitude)
    document.getElementById("lat").value = latitude
    document.getElementById("long").value = longitude
}


function print(mes){console.log(mes);}


function error(){

}


function addZero(num){
    if (Number(num)<10) {
        return "0" + num;
    }
    else {
        return num;
    }
}

function timeNowED(){

    document.getElementById("datetime-local").value = d.getFullYear()+"-"+ addZero(parseInt(d.getMonth())+1) +"-"+ addZero(d.getDate())+"T"+addZero(d.getHours())+":"+addZero(d.getMinutes());
    
}

function getGeolocation(){
    
    navigator.geolocation.getCurrentPosition(success, error);
}

