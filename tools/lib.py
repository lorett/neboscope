# -@*- coding: utf-8 -*-
#
# библиотека вспомогательных функций общего назначения и общие константы
#
#--------------------------------------------------------


API_KEY_LEN = 16 # длина API  ключа


def get_random_string(length,lower_case=False): 
	""" генерирует случайную строку заданной длины (из символов нижнего или верхнего регистра + цифры) """
	import random
	import string
	letters =  string.digits
	if lower_case : letters = letters + string.ascii_lowercase
	else: letters = letters + string.ascii_uppercase
	result_str = ''.join(random.choice(letters) for i in range(length))
	return result_str
	


