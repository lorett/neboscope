BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "images" (
	"imageID"	TEXT,
	"status"	TEXT,
	"sourceName"	TEXT,
	"timeUTC"	text,
	"timeLocal"	TEXT,
	"uploadDate"	text,
	"lat"	REAL,
	"long"	REAL,
	"imgWidth"	INTEGER,
	"imgHeigth"	INTEGER,
	"userID"	text,
	"clientIP"	TEXT,
	"camera"	TEXT,
	"image"	TEXT,
	"location"	TEXT,
	PRIMARY KEY("imageID")
);
CREATE TABLE IF NOT EXISTS "space_image" (
	"imageID"	TEXT,
	"img_time"	TEXT,
	"type"	TEXT,
	"bbox_xmin"	REAL,
	"bbox_xmax"	REAL,
	"bbox_ymin"	REAL,
	"bbox_ymax"	REAL,
	"format"	TEXT,
	"url"	TEXT,
	PRIMARY KEY("imageID")
);
CREATE TABLE IF NOT EXISTS "pv" (
	"pvid"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"parameter"	TEXT,
	"value"	TEXT,
	"objectID"	TEXT NOT NULL,
	"author"	TEXT,
	"changed"	TEXT,
	"note"	TEXT
);
CREATE TABLE IF NOT EXISTS "users" (
	"userID"	text,
	"userName"	TEXT,
	"login"	text,
	"email"	TEXT,
	"password"	text,
	"registerDate"	text,
	"admin"	INTEGER DEFAULT 0,
	"role"	TEXT DEFAULT 'user',
	"apiKey"	TEXT,
	"location"	TEXT,
	"tz"	NUMERIC,
	"homeLat"	REAL,
	"homeLong"	REAL,
	PRIMARY KEY("userID")
);

CREATE INDEX IF NOT EXISTS "photo_time" ON "images" (
	"timeUTC"
);
CREATE INDEX IF NOT EXISTS "photo_user" ON "images" (
	"userID"
);
CREATE INDEX IF NOT EXISTS "pv_objectid" ON "pv" (
	"objectID"
);
COMMIT;
