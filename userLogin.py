class User:
    def fromDB(self, id, dbase):
        self.user = dbase.getUserById(id)
        self.images = dbase.getImagesByUserID(self.user.userID)
        if self.user.admin:
            self.allImages = dbase.getAllImages()
            print(self.allImages)
        return self

    
    def create(self, user):
        self.user = user
        #self.is_anonymous = False
        #print(self.user)
        return self


    def is_authenticated(self):
        return True
    

    def is_active(self):
        return True


    def is_admin(self):
        return True if self.user.admin==1 else False


    def is_anonymous(self):
        #self.is_anonymous = False
        return False

    def get_id(self):
        #print("\n\n\n\n\n")
        #print(self.__user)
        return str(self.user.userID)


    def get_login(self):
        return str(self.user.login) if self.user else "Без имени"


    def get_email(self):
        return str(self.user.email) if self.user else "Без email"


    def get_images(self, mode=0):
        return self.allImages if mode else self.images
