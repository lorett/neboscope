#import sqlite3
from datetime import datetime
from os.path import join, abspath
from config import *

# new
from prettytable import PrettyTable
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Table, Column, DateTime, Integer, String, MetaData, Boolean, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker   
from sqlalchemy.orm import relationship

import tools.lib




path = abspath( join(__file__, "..", "data", DBfileName) ) 
DBManagerVersion = "1.0"

def dict_factory(cursor, row):
    d={}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]

    return d


# DB check
class DB:
    base = declarative_base()
    
    

    def __init__(self, dataBase=path, echo=False):

        self.engine = create_engine(f'sqlite:///{dataBase}', echo = echo)
        self.base.metadata.create_all(self.engine)
        self.base.metadata.bind = self.engine
        self.DBSession = sessionmaker(bind=self.engine)
    

    class User(base):  
        __tablename__ = 'users'  
        
        userID = Column('userID', String(40), primary_key=True, nullable=False)
        email = Column('email', String(40), nullable=False)
        login = Column('login', String(40), nullable=False)
        password = Column('password', String(40), nullable=False)  
        securityKey = Column('securityKey', String(40), nullable=True)
        admin = Column('admin', Boolean, default=0)

    
    class Image(base):  
        __tablename__ = 'images'  

        imageID = Column('imageID', Integer, primary_key=True, autoincrement=True)
        userID = Column('userID', String(40), nullable=False)

        latitude = Column('latitude', Float, nullable=False)
        longitude = Column('longitude', Float, nullable=False)

        imagePath = Column('imagePath', String(150), nullable=False)
        sourseName = Column('sourseName', String(40), default="unrecognized")

        location = Column('location', String(40), nullable=False)
        clientIP =  Column('clientIP', String(40), nullable=False)
        date = Column('date', String(40), default= lambda: datetime.now().strftime('%Y-%m-%dT%H:%M'))   
        uploadDate = Column('uploadDate', String(40), default= lambda: datetime.now().strftime('%Y-%m-%dT%H:%M'))


    class Station(base):  
        __tablename__ = 'stations'  

        stationID = Column('stationID', Integer, primary_key=True, autoincrement=True)
        userID = Column('userID', String(40), nullable=False)
        groupID = Column('groupID', String(40), nullable=False)

        # registration coordinates
        latitude = Column('latitude', Float, nullable=False)
        longitude = Column('longitude', Float, nullable=False)
        elevation = Column('elevation', Float, nullable=False)

        location = Column('location', String(40), nullable=False)
        clientIP =  Column('clientIP', String(40), nullable=False)

        registrationDate = Column('registrationDate', String(40), default= lambda: datetime.now().strftime('%Y-%m-%dT%H:%M'))   

        # last coordinates
        lastLatitude = Column('lastLatitude', Float, nullable=False)
        lastLongitude = Column('lastLongitude', Float, nullable=False)
        lastElevation = Column('lastElevation', Float, nullable=False)
        lastDate = Column('lastActivityDate', String(40), default= lambda: datetime.now().strftime('%Y-%m-%dT%H:%M'))
    

    #add sattelite data
    class SensorData(base):  
        __tablename__ = 'sensorData'  
        
        userID = Column('userID', String(40), primary_key=True, nullable=False)
        stationID = Column('stationID', String(40), primary_key=True, nullable=False)
        imageID = Column('imageID', String(40), nullable=False)

        temperature = Column('temperature', Float, nullable=False)
        pressure = Column('pressure', Float, nullable=False)
        humidity = Column('humidity', Float, nullable=False)
        windSpeed = Column('windSpeed', Float, nullable=False)
        windDirections = Column('windDirections', Float, nullable=False)

        magnetometerX = Column('magnetometerX', Float, nullable=False)
        magnetometerY = Column('magnetometerY', Float, nullable=False)
        magnetometerZ = Column('magnetometerZ', Float, nullable=False)  

        securityKey = Column('securityKey', String(40), nullable=True)
        lastDate = Column('lastDate', String(40), default= lambda: datetime.now().strftime('%Y-%m-%dT%H:%M'))
                   

    # new
    def getAllImages(self):
        session = self.DBSession()
        
        images = session.query(self.Image).all()

        session.close()

        return images


    # new
    def getImagesByUserID(self, userID):
        session = self.DBSession()
        
        images = session.query(self.Image).filter(self.Image.userID == userID).all()

        session.close()

        print(userID)

        return images


    # new
    def getImageByID(self, imageID):
        session = self.DBSession()
        
        images = session.query(self.Image).filter(self.Image.imageID == imageID).first()
        
        session.close()

        return images


    # new
    def getAllUsers(self):
        session = self.DBSession()
        
        users = session.query(self.User).all()

        session.close()

        return users


    def getUserById(self, userID):
        session = self.DBSession()
        
        user = session.query(self.User).filter(self.User.userID == userID).first()
        
        session.close()

        return user


    # new
    def getUserByLogin(self, login):
        session = self.DBSession()
        
        user = session.query(self.User).filter(self.User.login == login).first()
        
        session.close()

        return user


    # new  
    # Identify user by his apiKey
    def getUserByKey(self, apiKey):
        session = self.DBSession()
        
        user = session.query(self.User).filter(self.User.apiKey == apiKey).first()
        
        session.close()

        return user            
            

    #def fileLoadBD(self, userID, date, dateLoad, exifDate, lat, long, cameraInfo, exifLatitude, exifLongitude, ip, filename):
    #    try:
    #        conn = sqlite3.connect(self.path)
    #        cursor = conn.cursor()
    #        
    #        cursor.execute(f"""INSERT INTO images
    #                         VALUES ("{userID}", "{date}", "{dateLoad}", "{exifDate}", "{lat}",
    #                          "{long}", "{cameraInfo}", "{exifLatitude}", "{exifLongitude}",
    #                           "{ip}", "{filename}")""")    
    #        conn.commit()
    #        cursor.close()
    #        conn.close()
    #        return True
    #    
    #    except sqlite3.Error as e:
    #        print("err: ", str(e))
    #        return False


    def fileLoadBD(self, userID, latitude, longitude, imagePath, sourceImage, location, clientIP, date):
        #try:
        
        session = self.DBSession()
        
        print(f"{userID=} {latitude=} {longitude=} {imagePath=} {sourceImage=} {location=} {clientIP=} {date=} {datetime.now().strftime('%Y-%m-%dT%H:%M')}")
        session.add( self.Image( userID=userID, latitude=latitude, longitude=longitude, imagePath=imagePath, sourseName=sourceImage, location=location, clientIP=clientIP, date=date, uploadDate=datetime.now().strftime("%Y-%m-%dT%H:%M") ) )
        print(f"\n\nsas\n\n")
        session.commit()
        session.close()
        return True

        #except Exception as e:
        #    print("err: ", str(e))
        #    return False

    # new 
    def registrationDB(self, email, login, password, securityKey=None, admin=0):
        try:
            
            session = self.DBSession()

            userByLogin = session.query(self.User).filter( self.User.login == login ).first()
            userByEmail = session.query(self.User).filter( self.User.email == email ).first()

            if not userByLogin and not userByEmail:
                session.add(self.User(userID="@"+login, email=email, login=login, password=password, admin=admin))

                session.commit()
                session.close()

                return True

            else:
                session.commit()
                session.close()

                return False

        except Exception as e:
            print(e)
            return False


    # new 
    def loginDB(self, login, password):
        try:
            
            session = self.DBSession()

            user = session.query(self.User).filter(self.User.login == login).first()

            session.close()

            if user:
                if user.password == password:
                    print("$ Authorization was successful")
                    return True
                else:
                    print("$ acccess denied")
                    return False
            else:
                    print("$ user not found")
                    return False

        except Exception as e:
            print(e)
            return False


    # new
    def deleteByID(self, userID):
        try:
            
            session = self.DBSession()

            session.query(self.User).filter(self.User.userID == userID).delete()

            session.commit()
            session.close()
            return True
        
        except Exception as e:
            print(e)
            return False


    # new
    def deleteUserByLogin(self, login):
        
        try:
            
            session = self.DBSession()

            session.query(self.User).filter(self.User.login == login).delete()

            session.commit()
            session.close()
            return True
        
        except Exception as e:
            print(e)
            return False   



def getTable(users):
        th = ["UserID", "email", "login", "password", "securityKey", "admin"]
        td = []

        table = PrettyTable(th)

        for user in users:
            #res.append( self.userAnswers[i] == self.correctAnswers[i] )
            td.append([user.UserID, user.email, user.login, user.password[:8], user.securityKey[:8], user.admin])

        for i in td:
            table.add_row(i)

        return table


# console db manager
if __name__ == "__main__":
    
    while True:
        try:
            db = DB()
            command = ""

            print(f"db manadger {DBManagerVersion}")

            while not ("exit" in command):
                
                # email, login, password, securityKey, admin
                if command == "registration":
                    print()
                    print(db.registrationDB(email=input("email: "), login=input("login: "), password=input("password: "), securityKey=input("securityKey: "), admin=input("amdin: ")), end="\n\n")


                elif command == "login":
                    print()
                    print(db.loginDB(login=input("login: "), password=input("password: ")), end="\n\n")


                elif command == "deleteById":
                    print()

                    id = int(input("id: "))
                    user = db.getUserById(id)

                    print(user)
                    
                    if user:
                        print(user.email, user.login, user.registrationDate, sep="\t")
                        
                        if input("delete (y/n)? ") == "y":
                            print("delete.. ", end="")
                            print(db.deleteUserById(id=id), end="\n\n")

                        else:
                            print("Cancel", end="\n\n")

                    else:
                        print("user is not found", end="\n\n")


                elif command == "deleteByLogin":
                    print()

                    login = input("login: ")
                    user = db.getUserBylogin(login)

                    if user:
                        print(user.email, user.login, user.registrationDate, sep="\t")
                        
                        if input("delete (y/n)? ") == "y":
                            print("delete.. ", end="")
                            print(db.deleteUserByLogin(login=login), end="\n\n")

                        else:
                            print("Cancel", end="\n\n")

                    else:
                        print("user is not found", end="\n\n")
                

                elif command == "getImagesByUserID":
                    
                    print(db.getAllImages())
                    print('\n')                   

                
                elif command == "getUsersList":
                    print()
                    print("Users:")
                    print( getTable(db.getAllUsers()) )
                    print("\n")
                    #print("\nid","email", "login", "password", "registration date:", sep="\t", end="\n\n")
                    #for user in db.getUsersList():
                    #    print(user.id, user.email, user.login, user.password, user.registrationDate, sep="\t")
                    #print()


                else:
                    print()
                    print(
                        "Commands:",
                        "registration - add new user in db",
                        "login - try login in to your account",
                        "getUsersList - print all users in db",
                        "deleteById - delete user with id",
                        "deleteByLogin - delete user with login",
                        "exit - close dbManager",
                        "Any other command will display this help",
                        sep="\n",
                        end="\n\n")

                command = input()

        except Exception as e:
            print("[ERROR]:", e)
            print("Press ENTER for reboot")
            input()
            print("reboot..\n\n")
